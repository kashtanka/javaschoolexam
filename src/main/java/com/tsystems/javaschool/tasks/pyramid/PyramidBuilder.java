package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        // TODO : Implement your solution here

            int listSize = inputNumbers.size();

            //double a = 8 * listSize + 1;
            double discrSquareRoot = Math.sqrt(8 * listSize + 1);
            int rows;

            if (Math.round(discrSquareRoot) - discrSquareRoot != 0)
                throw new CannotBuildPyramidException();
            else {
                rows = (int) ((-1 + discrSquareRoot) / 2);
            }

            for (int i = 0; i < inputNumbers.size(); i ++) {
                if (inputNumbers.get(i) == null) {
                    throw new CannotBuildPyramidException();
                }
            }

            int columns = rows + rows - 1;
            int [][] arr = new int[rows][columns];
            int listPointer = inputNumbers.size() - 1;  // pointer to the last element of List
            int columnsPointer = columns - 1;           // from which column to start in a new line
            // int numsInRow  -- number of List elements in a row

            Collections.sort(inputNumbers);

            for (int i = rows - 1; i >= 0; i--) {
                for (int j = columnsPointer, numsInRow = i + 1; numsInRow > 0; j -= 2, numsInRow-- ) {
                    arr[i][j] = inputNumbers.get(listPointer);
                    listPointer--;
                }
                columnsPointer--;
            }

            return arr;
    }
}
