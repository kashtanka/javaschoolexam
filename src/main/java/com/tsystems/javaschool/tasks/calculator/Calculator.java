package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    private class Number {
        private final boolean isLong;
        private final long longValue;
        private final double doubleValue;

        private Number(long value) {
            this.isLong = true;
            this.longValue = value;
            this.doubleValue = value;
        }

        private Number(double value) {
            this.isLong = false;
            this.doubleValue = value;
            this.longValue = -1;
        }

        boolean isLong() {
            return isLong;
        }

        public long getLongValue() {
            return longValue;
        }

        public double getDoubleValue() {
            return doubleValue;
        }
    }

    private interface Operator {
        Number calculate(Number left, Number right);
    }

    private class Plus implements Operator {
        @Override
        public Number calculate(Number right, Number left) {
            if (left.isLong && right.isLong) {
                return new Number(left.longValue + right.longValue);
            } else {
                return new Number(left.doubleValue + right.doubleValue);
            }
        }
    }

    private class Minus implements Operator {
        @Override
        public  Number calculate(Number right, Number left) {
            if (left.isLong && right.isLong) {
                return new Number(left.longValue - right.longValue);
            } else {
                return new Number(left.doubleValue - right.doubleValue);
            }
        }
    }

    private class Multiplication implements Operator {
        @Override
        public  Number calculate(Number right, Number left) {
            if (left.isLong && right.isLong) {
                return new Number(left.longValue * right.longValue);
            } else {
                return new Number(left.doubleValue * right.doubleValue);
            }
        }
    }

    private class Division implements Operator {
        @Override
        public  Number calculate(Number right, Number left) throws ArithmeticException{
            if (Math.abs(right.doubleValue) < 1e-8) {
                    throw new ArithmeticException();
                }
                if ((left.isLong && right.isLong) && (left.doubleValue % right.doubleValue == 0)) {
                        return new Number(left.longValue / right.longValue);
                } else {
                    return new Number(left.doubleValue / right.doubleValue);
                }
        }
    }

    private static final Map<Character, Integer> priorities = new HashMap<>();

    static {
        priorities.put('*', 1);
        priorities.put('/', 1);
        priorities.put('+', 2);
        priorities.put('-', 2);
        priorities.put('(', 3);
        priorities.put(')', 3);
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement == "") {
            return null;
        }
        Stack st1 = new Stack();
        Stack st2 = new Stack();
        List list = new ArrayList();
        String temp;

        Pattern numberPattern = Pattern.compile("\\d+(\\.\\d+)?");
        Matcher m = numberPattern.matcher(statement);

        try {
            int i = 0;
            while (i < statement.length()) {
                if (Character.isDigit(statement.charAt(i))) {
                    if (m.find(i)) {
                        temp = m.group();
                        i += temp.length();
                        if (temp.contains(".")) {
                            list.add(new Number(Double.parseDouble(temp)));
                        } else {
                            list.add(new Number(Long.parseLong(temp)));
                        }

                    }
                } else {
                    switch (statement.charAt(i)) {
                        case '+':
                        case '-':
                        case '*':
                        case '/':
                            if (!st1.empty() && priorities.get(statement.charAt(i)) >= priorities.get(st1.peek())) {
                                list.add(st1.pop());
                            }
                            st1.push(statement.charAt(i));
                            break;
                        case '(':
                            st1.push(statement.charAt(i));
                            break;
                        case ')':
                            while (!st1.peek().equals('(')) {
                                list.add(st1.pop());
                            }
                            st1.pop();
                            break;
                        default: throw new ArithmeticException();
                    }
                    i++;
                }
            }
            if (st1.search('(') != -1) {
                return null;
            }
            while (!st1.empty()) {
                list.add(st1.pop());
            }
        } catch (ArithmeticException | EmptyStackException e) {
            return null;
        }

        for (int i = 0; i < list.size(); i++) {
            if (priorities.containsKey(list.get(i))) {
                Number result = null;
                try {
                    if (list.get(i).equals('+')) {
                        Plus plus = new Plus();
                        result = plus.calculate((Number) st2.pop(), (Number) st2.pop());
                    }
                    if (list.get(i).equals('-')) {
                        Minus minus = new Minus();
                        result = minus.calculate((Number) st2.pop(), (Number) st2.pop());
                    }
                    if (list.get(i).equals('*')) {
                        Multiplication mult = new Multiplication();
                        result = mult.calculate((Number) st2.pop(), (Number) st2.pop());
                    }
                    if (list.get(i).equals('/')) {
                        Division div = new Division();
                        result = div.calculate((Number) st2.pop(), (Number) st2.pop());
                    }
                    st2.push(result);
                } catch (EmptyStackException | ArithmeticException e) {
                    return null;
                }
            } else {
                st2.push(list.get(i));
            }
        }

        Number n = (Number) st2.pop();

        NumberFormat nf = new DecimalFormat("#.####");

        if (n.isLong) {
            return String.valueOf(n.longValue);
        } else {
            return String.valueOf(nf.format(n.doubleValue));
        }
    }
}
